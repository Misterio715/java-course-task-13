package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.service.IProjectTaskService;
import org.fmavlyutov.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) {
            return;
        }
        if (taskId == null || taskId.isEmpty()) {
            return;
        }
        if (!projectRepository.existsById(projectId)) {
            return;
        }
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) {
            return;
        }
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) {
            return;
        }
        if (taskId == null || taskId.isEmpty()) {
            return;
        }
        if (!projectRepository.existsById(projectId)) {
            return;
        }
        final Task task = taskRepository.findOneById(taskId);
        if (task == null ) {
            return;
        }
        task.setProjectId(null);;
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) {
            return;
        }
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) {
            taskRepository.removeById(task.getId());
        }
        projectRepository.removeById(projectId);
    }

}
