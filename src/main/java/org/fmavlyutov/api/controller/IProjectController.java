package org.fmavlyutov.api.controller;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

}
